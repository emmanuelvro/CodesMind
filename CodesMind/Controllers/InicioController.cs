﻿using CodesMind.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodesMind.Controllers
{
    public class InicioController : Controller
    {
        // GET: Inicio
        public ActionResult Index()
        {
            return View();
        }


        public JsonResult Contacto(Contacto con) {

            string msg = "Su mensaje fue recibido";
            return Json(msg, JsonRequestBehavior.AllowGet);

        }
    }
}
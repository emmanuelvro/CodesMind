﻿$(document).on("ready",function () {

    $(".contacto").on("click", function () {

        $nombre = $("#nombre").val();
        $asunto = $("#asunto").val();
        $email = $("#email").val();
        $msg = $("#msg").val();

        $frm = $("#contact-form");

        var message = function (msg) {
            $('<div class="ajax-form-alert alert heading fade in text-center">	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> ' + msg + '</div>')
                    .addClass($frm.data('message-class')).appendTo('.result');
           
        };
        console.log($frm.serializeArray());
        $.ajax({
            type: $frm.attr('method'),
            url: '/Inicio/Contacto',
            data: $frm.serializeArray(),
            success: function (data) {
               // $frm[0].reset();
                message(data);
            },
            error: function (xhr, str) {
                message('Error: ' + xhr.responseCode);
            }
        });


//        alert($nombre + ' ' + $asunto + ' ' + $email + ' ' + $msg);

    });

});